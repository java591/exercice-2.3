package fr.cnam.foad.nfa035.badges.wallet.model;

import java.util.Objects;

public class DigitalBadgeMetadata {
    /**
     * couche modèle métadonnées
     */
    private int badgeId;
    private long imageSize;
    private long walletPosition;

    public DigitalBadgeMetadata(){}

    /**
     * constructeur
     * @param badgeId
     * @param imageSize
     * @param walletPosition
     */

    public DigitalBadgeMetadata(int badgeId,long imageSize,long walletPosition){
        this.badgeId=badgeId;
        this.imageSize=imageSize;
        this.walletPosition=walletPosition;
    }

    /**
     * comparaison avec equals
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    /**
     * getters
     * @return
     */
    public int getBadgeId() {return badgeId;}
    public long getImageSize() {return imageSize;}
    public long getWalletPosition(){return walletPosition;}

    /**
     * redéfinition de la méthode hashcode(contrat de comparaison equals)
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(badgeId, imageSize, walletPosition);
    }

    /**
     * setters
     * @param badgeId
     */
    public void setBadgeId(int badgeId){this.badgeId=badgeId;}
    public void setBadgeId(long imageSize){this.imageSize=imageSize;}
    public void setWalletPosition(long walletPosition){this.walletPosition=walletPosition;}

}
