package fr.cnam.foad.nfa035.badges.wallet.dao.impl;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.WalletFrame;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageDeserializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db.WalletSerializerDirectAccessImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.List;

/**
 * pattern DAO
 * accès aux données
 */

public class DirectAccessBadgeWalletDAOImpl implements DirectAccessBadgeWalletDAO {

    private final Logger LOG = (Logger) LogManager.getLogger(DirectAccessBadgeWalletDAOImpl.class);

    private ImageStreamingDeserializer<ResumableImageFrameMedia>deserializer;
    private final File walletDatabase;

    public DirectAccessBadgeWalletDAOImpl(String dbPath) throws IOException {
        this.walletDatabase = new File(dbPath);
    }

    /**
     * ajout d'un badge et serialsation
     * @param image
     * @throws IOException
     */
    @Override
    public void addBadge(File image) throws IOException {
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rw"))) {
            WalletSerializerDirectAccessImpl serializer=new WalletSerializerDirectAccessImpl();
            serializer.serialize(image,media);
        }
    }

    /**
     * retourne un badge
     * @param imageStream
     * @throws IOException
     */
    @Override
    public void getBadge(OutputStream imageStream) throws IOException {
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
            new ImageDeserializerBase64DatabaseImpl(imageStream).deserialize(media);
        }
    }

    /**
     * retourne une liste d'objet DigitalMetadata contenant les metadonnées
     * @return
     * @throws IOException
     */
    @Override
    public List<DigitalBadgeMetadata> getWalletMetadata() throws IOException {
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
            // A COMPLéTER
        }
    }

    /**
     * affiche un badge en fonction d'un objet métadonnées
     * @param imageStream
     * @param meta
     * @throws IOException
     */

    @Override
    public void getBadgeFromMetadata(OutputStream imageStream, DigitalBadgeMetadata meta) throws IOException {
        List<DigitalBadgeMetadata> metas = this.getWalletMetadata();
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rws"))) {
            // A COMPLéTER
        }
    }
}
