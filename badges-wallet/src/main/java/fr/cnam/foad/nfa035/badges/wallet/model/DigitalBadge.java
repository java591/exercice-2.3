package fr.cnam.foad.nfa035.badges.wallet.model;

import java.io.File;
import java.util.Objects;

public class DigitalBadge {

    /**
     * couche modèle badge
     */



    private File badge;
    private DigitalBadgeMetadata metadata;

    public DigitalBadge(File badge){this.badge=badge;}

    /**
     * comparaison avec equals
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    /**
     * getters
     * @return
     */
    public File getBadge(){return this.badge;}
    public DigitalBadgeMetadata getMetadata(){return metadata;}

    /**
     * contrat hashcode (méthode equals)
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(badge, metadata);
    }

    /**
     * setters
     */
    public void setBadge(){}
    public void setMetadata(){}

    public String toString(){return badge.toString();}
}
