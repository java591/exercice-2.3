package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.DirectAccessDatabaseDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class WalletDeserializerDirectAccessImpl implements DirectAccessDatabaseDeserializer {

    private final Logger LOG = (Logger) LogManager.getLogger(WalletDeserializerDirectAccessImpl.class);

    private List<DigitalBadgeMetadata> meta;

    private OutputStream sourceOutputStream;

    public WalletDeserializerDirectAccessImpl(){

    }

    @Override
    public void deserialize(WalletFrameMedia media) throws IOException {

    }
    @Override
    public void deserialize(WalletFrameMedia media, DigitalBadgeMetadata meta) throws IOException {
        long pos = meta.getWalletPosition();
        media.getChannel().seek(pos);
        String[] data = media.getEncodedImageReader(false).readLine().split(";");
        try (OutputStream os = getSourceOutputStream()) {
            getDeserializingStream(data[3]).transferTo(os);
        }
    }


    @Override
    public <K extends InputStream> K getDeserializingStream(String data) throws IOException {
        return null;
    }

    @Override
    public <T extends OutputStream> T getSourceOutputStream() {
        return null;
    }

    @Override
    public <T extends OutputStream> void setSourceOutputStream(T os) {

    }
}
